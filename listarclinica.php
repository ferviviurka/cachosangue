<?php
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL,"https://localhost:44346/api/Usuarios");
  $result=curl_exec($ch);
  curl_close($ch);
  $result=json_decode($result);
?>
<?php
    include('headeradmin.php');
?>
<br><br><br>
<section id="mainadmin">
 <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Listar <b>Doadores</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Adicionar novo <b>Doador</b></span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Login</th>
						<th>Senha</th>
                        <th>Cpf</th>
                        <th>Email</th>
                        <th>Telefone</th>
                        <th>Padrao de Acesso</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php
                                foreach ($result as $usuario) {
					?>
					<tr>
                        <td>
                            <?php
                                echo $usuario->idUsuario . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Nome . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Login . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Senha . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Cpf . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Email . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Fone . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->PadraoAcesso . "<br>";
                            ?>
                        </td>
						<td>
                            <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Remover</span></a>						
                        </td>
						<?php
                                }
                            ?>
                        
                            
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
	<!-- Edit Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form action="controller/enviardoador.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Adicionar <b>Doador</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome</label>
									<input type="text" class="form-control" placeholder="Nome" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="entrar">Login</label>
								<input type="text" class="form-control" placeholder="Login" name="entrar" id="entrar">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Senha">Senha</label>
									<input type="text" class="form-control" placeholder="Senha" name="senha" id="senha">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="cpf">CPF</label>
									<input type="text" class="form-control" placeholder="CPF" name="cpf" id="cpf">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="telefone">Telefone</label>
									<input type="text" class="form-control" placeholder="Telefone" name="telefone" id="telefone">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Email">Email</label>
									<input type="text" class="form-control" placeholder="Email" name="email" id="email">
								</div>
							</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Adicionar">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Edit Modal HTML -->
	<div id="editEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Edit Employee</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control" required></textarea>
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" class="form-control" required>
						</div>					
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-info" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Delete Employee</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>Are you sure you want to delete these Records?</p>
						<p class="text-warning"><small>This action cannot be undone.</small></p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" value="Delete">
					</div>
				</form>
			</div>
		</div>
	</div>
    </section>
