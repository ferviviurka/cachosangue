<?php
session_start();
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL,"https://localhost:44346/api/Usuarios");
  $result=curl_exec($ch);
  curl_close($ch);
  $result=json_decode($result);
?>
<?php
    include('headeradmin.php');
?>
<br><br><br>
<section id="mainadmin">
 <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Listar <b>Doadores</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Adicionar novo <b>Doador</b></span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Login</th>
						<th>Senha</th>
                        <th>Cpf</th>
                        <th>Email</th>
                        <th>Telefone</th>
                        <th>Padrao de Acesso</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php
                        foreach ($result as $usuario) {
					?>
					<tr>
                        <td>
                            <?php
								echo $usuario->idUsuario . "<br>";
								$userid = $usuario->idUsuario;
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Nome . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Login . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Senha . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Cpf . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Email . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Fone . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->PadraoAcesso . "<br>";
                            ?>
                        </td>
						<td>
						 <button type="submit" class="btn btn-danger" onclick="removervalue(<?php echo $usuario->idUsuario?>)" id="botao">
						 	<i class="material-icons">&#xE15C;</i> <span>Remover</span>
						 </button>	
						
						
						</td>
						<td>
                        <button type="submit" class="btn btn-warning" href="#remove" class="btn btn-success" data-toggle="modal">
						 	<i class="material-icons">&#xE15C;</i> <span>Editar</span>
						 </button>		
                        </td>
						<?php
								}
								
                            ?>
                        
                            
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
	<!-- Cadastrar Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form action="controller/enviardoador.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Adicionar <b>Doador</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome</label>
									<input type="text" class="form-control" placeholder="Nome" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="entrar">Login</label>
								<input type="text" class="form-control" placeholder="Login" name="entrar" id="entrar">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Senha">Senha</label>
									<input type="text" class="form-control" placeholder="Senha" name="senha" id="senha">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="cpf">CPF</label>
									<input type="text" class="form-control" placeholder="CPF" name="cpf" id="cpf">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="telefone">Telefone</label>
									<input type="text" class="form-control" placeholder="Telefone" name="telefone" id="telefone">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Email">Email</label>
									<input type="text" class="form-control" placeholder="Email" name="email" id="email">
								</div>
							</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Adicionar">
					</div>
				</form>
			</div>
		</div>
	</div>
    </section>


	<div id="remove" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form action="controller/alterardoador.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Alterar <b>Doador</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="iddoador">Id</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->idUsuario?>" name="iddoador" id="iddoador" value="<?php echo $usuario->idUsuario?>" disabled>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->Nome?>" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="entrar">Login</label>
								<input type="text" class="form-control" placeholder="<?php echo $usuario->Login?>" name="entrar" id="entrar">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Senha">Senha</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->Senha?>" name="senha" id="senha">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="cpf">CPF</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->Cpf?>" name="cpf" id="cpf">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="telefone">Telefone</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->Fone?>" name="telefone" id="telefone">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="Email">Email</label>
									<input type="text" class="form-control" placeholder="<?php echo $usuario->Email?>" name="email" id="email">
								</div>
							</div>
					</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<button type="submit" class="btn btn-danger">
						 	<i class="material-icons">&#xE15C;</i> <span>Alterar</span>
						 </button>
					</div>
				</form>
			</div>
		</div>
	</div>
    </section>


<script>
function removervalue(userid){

	window.location.href = "controller/excluirdoador.php/"+userid;
}

</script>