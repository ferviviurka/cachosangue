<?php
    include('header.php');
?>

<section id="homeee">
<div class="box-admin-clinica">
    <form>
    <h2>Bem vindo ao painel de Admin</h2>
    </div>

<div class="container">
    <div class="box-cadastro-clinica">
    <form>
    <h2>Cadastro Clínica</h2>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="first">Nome da Clínica</label>
          <input type="text" class="form-control" placeholder="Nome" id="first">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="last">Endereço</label>
          <input type="text" class="form-control" placeholder="Sobrenome" id="last">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="company">Cep</label>
          <input type="text" class="form-control" placeholder="Idade" id="idade">
        </div>
      </div>
    </div>
        <button type="submit" class="btn btn-dark" style="margin-left:45px;">Cadastrar</button>
  </form>
    </div>
</div>
<br><br><br>
<svg  class="wave2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#273036" fill-opacity="1" d="M0,192L18.5,160C36.9,128,74,64,111,74.7C147.7,85,185,171,222,213.3C258.5,256,295,256,332,234.7C369.2,213,406,171,443,170.7C480,171,517,213,554,192C590.8,171,628,85,665,80C701.5,75,738,149,775,197.3C812.3,245,849,267,886,250.7C923.1,235,960,181,997,186.7C1033.8,192,1071,256,1108,282.7C1144.6,309,1182,299,1218,256C1255.4,213,1292,139,1329,106.7C1366.2,75,1403,85,1422,90.7L1440,96L1440,320L1421.5,320C1403.1,320,1366,320,1329,320C1292.3,320,1255,320,1218,320C1181.5,320,1145,320,1108,320C1070.8,320,1034,320,997,320C960,320,923,320,886,320C849.2,320,812,320,775,320C738.5,320,702,320,665,320C627.7,320,591,320,554,320C516.9,320,480,320,443,320C406.2,320,369,320,332,320C295.4,320,258,320,222,320C184.6,320,148,320,111,320C73.8,320,37,320,18,320L0,320Z"></path></svg>

</section>

<?php
    include('footer.php');
?>