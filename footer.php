<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>
<footer class="mainfooter" role="contentinfo">
  <div class="footer-middle">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6">
        <div class="footer-pad">
          <h4>Institucional</h4>
          <ul class="list-unstyled">
            <li><a href="#">Política de privacidade e segurança</a></li>
            <li><a href="#">Seja um doador</a></li>
            <li><a href="#">Seja um receptor</a></li>
            <li><a href="#">Sobre nós</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contato</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
    		<h4>Conecte-se a nós</h4>
            <ul class="social-network social-circle">
             <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
             <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
             <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-instagram"></i></a></li>
             <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-instagram"></i></a></li>


            </ul>				
        </div>
        <br>
      <div class="col-md-4 col-sm-6">
        <div class="footer-pad">
          <h4>CachoSangue</h4>
          <ul class="list-unstyled">
            <li><a>CACHOSANGUE TECNOLOGIA DA INFORMAÇÃO LTDA</a></li>
            <li><a href="#">CNPJ: 12.345.678/0001-23</a></li>
            <li><a href="#">Av. Casa do Ghisi, 128/134 - Boqueirão</a></li>
            <li><a href="#">Curitiba, Paraná</a></li>
            <li><a href="#">Tel: (41) 9999-9999</a></li>
        </ul>
        </div>
      </div>	
    </div>
	<div class="row">
		<div class="col-md-12 copy">
			<p class="text-center">&copy; Copyright 2020 - CachoSangue.  Todos os direitos reservados.</p>
		</div>
	</div>
  </div>
  </div>
</footer>
  
  
  </body>
</html>
</html>