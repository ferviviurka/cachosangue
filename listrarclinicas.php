<?php
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL,"https://localhost:44346/api/Clinicas");
  $result=curl_exec($ch);
  curl_close($ch);
  $result=json_decode($result);
?>
<?php
    include('headeradmin.php');
?>
<br><br><br>
<section id="mainadmin">
 <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Listar <b>Clinicas</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Adicionar nova <b>Clínica</b></span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id </th>
                        <th>Nome Clinica</th>
                        <th>Endereço</th>
						<th>CEP</th>    
                        <th>Remover</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php
                    foreach ($result as $usuario) {
					?>
					<tr>
                        <td>
                            <?php
                                echo $usuario->IdClinica . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->NomeClinica . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Endereco . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Cep . "<br>";
                            ?>
                        </td>
						<td>
                        <button type="submit" class="btn btn-danger" onclick="removervalue(<?php echo $usuario->IdClinica?>)" id="botao">
						 	<i class="material-icons">&#xE15C;</i> <span>Remover</span>
						 </button>			
                        </td>
                        <td>
                        <button type="submit" class="btn btn-warning">
						 	<i class="material-icons">&#xE15C;</i> <span>Editar</span>
						 </button>		
                        </td>
						<?php
                                }
                            ?>
                        
                            
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
	<!-- Cadastrar Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form action="controller/enviarclinica.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Adicionar <b>Clinica</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome Clinica</label>
									<input type="text" class="form-control" placeholder="Nome Cachorro" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="endereco">Endereço</label>
								<input type="text" class="form-control" placeholder="Endereço" name="endereco" id="endereco">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="cep">CEP</label>
									<input type="text" class="form-control" placeholder="CEP" name="cep" id="cep">
								</div>
							</div>
							
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Adicionar">
					</div>
				</form>
			</div>
		</div>
	</div>
    </section>

    <script>
function removervalue(userid){

	window.location.href = "controller/excluirclinica.php/"+userid;
}
</script>