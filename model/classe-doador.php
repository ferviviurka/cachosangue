<?php
include '../../config/Conexao.php';


    class Doador{

        private $connection;
        private $table_name = "Usuario";


        public $idUsuario;
        public $nome;
        public $login; 
        public $senha; 
        public $cpf; 
        public $email;
        public $telefone;
        public $padraoAcesso;
        

        public function getidUsuario()
        {
            return $this->idUsuario;
        }
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($nome)
        {
            $this->nome = $nome;
        }
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($login)
        {
            $this->login = $login;
        }
        public function getSenha()
        {
            return $this->senha;
        }
        public function setSenha($senha)
        {
            $this->senha = $senha;
        }
        public function getCpf()
        {
            return $this->cpf;
        }
        public function setCpf($cpf)
        {
            $this->cpf = $cpf;
        }
        public function getTelefone()
        {
            return $this->telefone;
        }
        public function setTelefone($telefone)
        {
            $this->telefone = $telefone;
        }
        public function getEmail()
        {
            return $this->email;
        }
        public function setEmail($email)
        {
            $this->email = $email;
        }
        public function getPadraoAcesso()
        {
            return $this->padraoAcesso;
        }
        public function setPadraoAcesso($padraoAcesso)
        {
            $this->padraoAcesso = $padraoAcesso;
        }

        public function __construct($connection){
            $this->connection = $connection;
        }
        
        public function create(){

        }

        public function read(){

        }
        
        public function update(){

        }

        public function delete(){

        }
    
    }
?>