<?php
session_start();
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL,"https://localhost:44346/api/Cachorroes");
  $result=curl_exec($ch);
  curl_close($ch);
  $result=json_decode($result);
?>
<?php
    include('headeradmin.php');
?>
<br><br><br>
<section id="mainadmin">
 <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Listar <b>Cachorros</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Adicionar novo <b>Cachorro</b></span></a>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name Cachorro</th>
                        <th>Idade</th>
						<th>Raça</th>
                        <th>Peso</th>
                        <th>Tamanho</th>
                        <th>Doença</th>
                        <th>Sexo</th>
                        <th>Cor</th>
                        <th>Tipo Sanguineo</th>
                        <th>id Usuário</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php
                                foreach ($result as $usuario) {
					?>
					<tr>
                        <td>
                            <?php
                                echo $usuario->IdCachorro . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->NomeCachorro . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Idade . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Raca . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Pesoa . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Tamanho . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Doenca . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Sexo . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->Cor . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->TipoSanguineo . "<br>";
                            ?>
                        </td>
                        <td>
                            <?php
                                echo $usuario->IdUsuario . "<br>";
                            ?>
                        </td>
						<td>
                        <button type="submit" class="btn btn-danger" onclick="removervalue(<?php echo $usuario->IdCachorro?>)" id="botao">
						 	<i class="material-icons">&#xE15C;</i> <span>Remover</span>
						 </button>			
                        </td>
                        <td>
                        <button type="submit" class="btn btn-warning">
						 	<i class="material-icons">&#xE15C;</i> <span>Editar</span>
						 </button>		
                        </td>
						<?php
                                }
                            ?>
                        
                            
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
	<!-- Cadastrar Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form action="controller/enviarcachorro.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Adicionar <b>Cachorro</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome Cachorro</label>
									<input type="text" class="form-control" placeholder="Nome Cachorro" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="idade">Idade</label>
								<input type="text" class="form-control" placeholder="Idade" name="idade" id="idade">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="raca">Raça</label>
									<input type="text" class="form-control" placeholder="Raça" name="raca" id="raca">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="peso">Peso</label>
									<input type="text" class="form-control" placeholder="CPF" name="peso" id="peso">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tamanho">Tamanho</label>
									<input type="text" class="form-control" placeholder="Tamanho" name="tamanho" id="tamanho">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="doenca">Doença</label>
									<input type="text" class="form-control" placeholder="Doença" name="doenca" id="doenca">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="sexo">Sexo</label>
									<input type="text" class="form-control" placeholder="Sexo" name="sexo" id="sexo">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="cor">Cor</label>
									<input type="text" class="form-control" placeholder="Cor" name="cor" id="cor">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="sangue">Tipo Sanguineo</label>
									<input type="text" class="form-control" placeholder="Tipo Sanguineo" name="sangue" id="sangue">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="idusuario">Id Usuário</label>
									<input type="text" class="form-control" placeholder="Id Usuário" name="idusuario" id="idusuario">
								</div>
							</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Adicionar">
					</div>
				</form>
			</div>
		</div>
	</div>
    </section>
    <script>
function removervalue(userid){

	window.location.href = "controller/excluircachorro.php/"+userid;
}
</script>