<?php
    include('headeruser.php');
?>

<section id="homee">
  <br><br><br>
<div class="container">
<div class="modal-content">
			<form action="controller/enviarcachorro.php" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Adicionar <b>Cachorro</b></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
    						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									<label for="nome">Nome Cachorro</label>
									<input type="text" class="form-control" placeholder="Nome Cachorro" name="nome" id="nome">
								</div>
							</div>
							<div class="col-md-12">
									<div class="form-group">
								<label for="idade">Idade</label>
								<input type="text" class="form-control" placeholder="Idade" name="idade" id="idade">
									</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="raca">Raça</label>
									<input type="text" class="form-control" placeholder="Raça" name="raca" id="raca">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="peso">Peso</label>
									<input type="text" class="form-control" placeholder="CPF" name="peso" id="peso">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="tamanho">Tamanho</label>
									<input type="text" class="form-control" placeholder="Tamanho" name="tamanho" id="tamanho">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="doenca">Doença</label>
									<input type="text" class="form-control" placeholder="Doença" name="doenca" id="doenca">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="sexo">Sexo</label>
									<input type="text" class="form-control" placeholder="Sexo" name="sexo" id="sexo">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="cor">Cor</label>
									<input type="text" class="form-control" placeholder="Cor" name="cor" id="cor">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="sangue">Tipo Sanguineo</label>
									<input type="text" class="form-control" placeholder="Tipo Sanguineo" name="sangue" id="sangue">
								</div>
							</div>
                            <div class="col-md-12">
								<div class="form-group">
									<label for="idusuario">Id Usuário</label>
									<input type="text" class="form-control" placeholder="Id Usuário" name="idusuario" id="idusuario">
								</div>
							</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Adicionar">
					</div>
				</form>
</div>

</section>
<svg  class="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#273036" fill-opacity="1" d="M0,192L18.5,160C36.9,128,74,64,111,74.7C147.7,85,185,171,222,213.3C258.5,256,295,256,332,234.7C369.2,213,406,171,443,170.7C480,171,517,213,554,192C590.8,171,628,85,665,80C701.5,75,738,149,775,197.3C812.3,245,849,267,886,250.7C923.1,235,960,181,997,186.7C1033.8,192,1071,256,1108,282.7C1144.6,309,1182,299,1218,256C1255.4,213,1292,139,1329,106.7C1366.2,75,1403,85,1422,90.7L1440,96L1440,320L1421.5,320C1403.1,320,1366,320,1329,320C1292.3,320,1255,320,1218,320C1181.5,320,1145,320,1108,320C1070.8,320,1034,320,997,320C960,320,923,320,886,320C849.2,320,812,320,775,320C738.5,320,702,320,665,320C627.7,320,591,320,554,320C516.9,320,480,320,443,320C406.2,320,369,320,332,320C295.4,320,258,320,222,320C184.6,320,148,320,111,320C73.8,320,37,320,18,320L0,320Z"></path></svg>

<?php
    include('footer.php');
?>